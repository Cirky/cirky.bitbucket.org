
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Kreiraj nov EHR zapis za pacienta in dodaj osnovne demografske podatke.
 * V primeru uspešne akcije izpiši sporočilo s pridobljenim EHR ID, sicer
 * izpiši napako.
 */
function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val() + "T00:00:00.000Z";
	var krvniTlakSistolicni = $("#kreirajKrvniTlakSistolicni").val();
	var krvniTlakDiastolicni = $("#kreirajKrvniTlakDiastolicni").val();
	
	
	if (!ime || !priimek || !datumRojstva || !krvniTlakSistolicni || !krvniTlakDiastolicni || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0 || krvniTlakSistolicni.trim().length == 0 || krvniTlakDiastolicni.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		       
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "bloodPressureS", value: krvniTlakSistolicni}, {key: "bloodPressureD", value: krvniTlakDiastolicni}, {key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party", // da vemo, kakšna je forma, moramo pogledati dokumentacijo na ehr scape
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {  // uspešno
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                    
		                    
		                    var datumInUra = "2018-11-21T11:40Z";
							var telesnaVisina = "199";
							var telesnaTeza = "99";
							var telesnaTemperatura = "36.50";
							var sistolicniKrvniTlak = krvniTlakSistolicni;
							var diastolicniKrvniTlak = krvniTlakDiastolicni;
							var nasicenostKrviSKisikom = "98";
							var merilec = "Maj Zmaj";
		                    
		                    
							$.ajaxSetup({
							    headers: {"Ehr-Session": sessionId}
							});
								var podatki = { // source predloge za vital signs
									// Preview Structure: https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
								    "ctx/language": "en",
								    "ctx/territory": "SI",
								    "ctx/time": datumInUra,
								    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
								    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
								   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
								    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
								    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
								    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
								    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
								};
							var parametriZahteve = {
							    ehrId: ehrId,
							    templateId: 'Vital Signs',
							    format: 'FLAT',
							    commiter: merilec
							};
							$.ajax({
							    url: baseUrl + "/composition?" + $.param(parametriZahteve),
							    type: 'POST',
							    contentType: 'application/json',
							    data: JSON.stringify(podatki),
							    success: function (res) {
							        $("#dodajMeritveVitalnihZnakovSporocilo").html(
					              "<span class='obvestilo label label-success fade-in'>" +
					              res.meta.href + ".</span>");
							    },
							    error: function(err) {
							    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
					            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
					            JSON.parse(err.responseText).userMessage + "'!");
							    }
							});		                    
		                    
		                }
		                
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}


/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in krvni tlak).
 */
function preberiKrvniTlakodBolnika() {
	sessionId = getSessionId();

	
	var krvniTlakS;
	var krvniTlakD;
	
	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		
			$.ajax({
				url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
		    	type: 'GET',
		    	headers: {"Ehr-Session": sessionId},
		    	success: function (data) {
		    	//	console.log(data);
		    		var ime = data.party.firstNames;
		    		var priimek = data.party.lastNames;
		    		var datumRojstva = data.party.dateOfBirth.split("-");
		    		var dan = parseInt(datumRojstva[2]);
		    		var mesec = parseInt(datumRojstva[1]);
		    		var leto = parseInt(datumRojstva[0]);
		    		var starost = 0;
		    		
		    		var today = new Date();
					var dd = today.getDate();
					var mm = today.getMonth()+1; //January is 0!
					var yyyy = today.getFullYear();
					
					if(dd<10) {
					    dd = '0'+dd
					} 
					
					if(mm<10) {
					    mm = '0'+mm
					} 
				//	console.log(dd + " " + mm + " " + yyyy);
		    	//	console.log(datumRojstva);
		    		if((dan - dd <= 0 && mesec - mm <= 0) || (mesec - mm < 0)){
		    			
		    			starost = yyyy - leto;
		    		}
		    		else
		    			starost = yyyy - leto - 1;
		    		
		    		
		    		$("#pacient").html("<br><br>Pacient: <b>" + ime + " " + priimek + ", " + starost + "</b>.");
		    		
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (data) {
					    	krvniTlakS = data[0].systolic;
					    	krvniTlakD = data[0].diastolic;
					    	
					    	$("#pacientov-krvni-tlak").html("<br><br>Krvni tlak: <b>" + krvniTlakS + "/" + krvniTlakD + " " + data[0].unit + "</b>.");
					//    	console.log(data);
					
							if(krvniTlakS < 90 || krvniTlakD < 60 ){
									$("#Nasvet").html("<br><br>Imate nizek krvni tlak. To običajno ni problem, vendar pa če se počutite slabo in imate občutek, da je prenizek, potem obiščite zdravnika. <b></b>");
									
							}
							else if(krvniTlakS < 120 && krvniTlakD < 80 ){
									$("#Nasvet").html("<br><br>ODLIČNO! Vaš krvni tlak je normalen.<b></b>");
							}
							else if(krvniTlakS < 130 && krvniTlakD < 80 ){
									$("#Nasvet").html("<br><br>Imate povečan krvni pritisk. Predlagamo naslednje rešitve: imejte dobro uteženo prehrano (čim manj soli ter sladkorja), omejite uživanje alkoholnik pijač, redno telovadite, prenehajte kaditi in obiščite zdravnika za nasvet.<b></b>");
							}	
							else if(krvniTlakS >= 180 || krvniTlakD >= 120 ){
									$("#Nasvet").html("<br><br>OPOZORILO!!! Počakajte 5 minut in ponovno izmerite krvni tlak. Če se tako visoka meritev ponovi, potem obstaja velika možnost, da boste padli v hipertenzivno krizo. Nemudoma kontaktirajte zdravnika oz. 112! <b></b>");
							}
							else if(krvniTlakS < 140 || krvniTlakD < 90 ){
									$("#Nasvet").html("<br><br>POZOR! Imate visok krvni pritisk (Hipertenzija stopnje 1). Predlagamo čim hitrejši obisk zdravnika, ki vam bo lahko podal več podatkov ter po potrebi predpisal zdravila.<b></b>");
							}							
							else if(krvniTlakS < 180 || krvniTlakD < 120 ){
									$("#Nasvet").html("<br><br>POZOR! Imate visko krvni pritisk (Hipertenzija stopnje 2). Nujno obiščite zdravnika, da vam predpiše zdravila ter predlaga nadaljni življenski stil<b></b>");
							}							
					
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
		    	}	
			});	
	}
}

function generirajPodatke(stPacienta){
	//console.log ("lala");
	sessionId = getSessionId();
	//var ehrId = "";
	$.ajaxSetup({
		headers: {"Ehr-Session": sessionId}
	});
	$.ajax({
	    url: baseUrl + "/ehr",
	    type: 'POST',
	    success: function (data) {
	        var ehrId = data.ehrId;
	        //$("#header").html("EHR: " + ehrId);
			//$("#Generiranec1").innerHTML = ehrId;
	        // build party data
	        if (stPacienta == 1) {
		        var partyData = {
		            firstNames: "Lolek",
		            lastNames: "Bolek",
		            dateOfBirth: "2002-07-18",
		            partyAdditionalInfo: [
		                {
		                    key: "ehrId",
		                    value: ehrId
		                }
		            ]
		        };
		        var podatki = [
		        	{
		        		"ctx/language": "en",
					    "ctx/territory": "SI",
					    "ctx/time": "1980-11-21T11:40Z",
					    "vital_signs/blood_pressure/any_event/systolic": "70",
					    "vital_signs/blood_pressure/any_event/diastolic": "50"
		        	}
	        	];
	        }
	        else if (stPacienta == 2) {
		        var partyData = {
		            firstNames: "Janez",
		            lastNames: "Janez",
		            dateOfBirth: "1970-07-18",
		            partyAdditionalInfo: [
		                {
		                    key: "ehrId",
		                    value: ehrId
		                }
		            ]
		        };
		        var podatki = [
		        	{
		        		"ctx/language": "en",
					    "ctx/territory": "SI",
					    "ctx/time": "1980-11-21T11:40Z",
					    "vital_signs/blood_pressure/any_event/systolic": "180",
					    "vital_signs/blood_pressure/any_event/diastolic": "123"
		        	}
	        	];
	        }
	        else if (stPacienta == 3) {
		        var partyData = {
		            firstNames: "Gloria",
		            lastNames: "Borger",
		            dateOfBirth: "2000-01-01",
		            partyAdditionalInfo: [
		                {
		                    key: "ehrId",
		                    value: ehrId
		                }
		            ]
		        };
		        var podatki = [
		        	{
					    "ctx/language": "en",
					    "ctx/territory": "SI",
					    "ctx/time": "1980-11-21T11:40Z",
					    "vital_signs/blood_pressure/any_event/systolic": "119",
					    "vital_signs/blood_pressure/any_event/diastolic": "69"
		        	}
	        	];
	        }
	        if (podatki){
		        var parametriZahteve = {
				    ehrId: ehrId,
				    templateId: 'Vital Signs',
				    format: 'FLAT',
				    //committer: merilec
				};
				for (var i = 0; i<podatki.length; i++){
			        $.ajax({
					    url: baseUrl + "/composition?" + $.param(parametriZahteve),
					    type: 'POST',
					    contentType: 'application/json',
					    data: JSON.stringify(podatki[i]),
					 
					    success: function (res) {
					    },
					    error: function(err) {
					    	$("#noviGeneriraneci").html(
				            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
				            JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
	    		}
	        }
	        //console.log(partyData);
	        $.ajax({
	            url: baseUrl + "/demographics/party",
	            type: 'POST',
	            contentType: 'application/json',
	            data: JSON.stringify(partyData),
	            success: function (party) {
                	if (stPacienta == 1){
                		$("#Generiranec1").html("Lolek Bolek: " + "<i>"+ehrId+"</i>");
                		if ($("#noviGeneriraneci").val() == "")
                			$("#noviGeneriraneci").html("<span style = 'color: green'><h3><b>Generirani EhrId-ji:</b></h3></span>");
                		$("#novGeneriranec1").html("<span><b>Lolek Bolek: " + ehrId +"</b></span>");
                		$("#ehrId1").html(ehrId);
                		
                		var sel = document.getElementById("preberiObstojeciEHR");
						var opt = document.getElementById("1");
						opt.value = ehrId;
						opt.text = "Lolek Bolek";
						sel.add(opt, null);                		
                	}
                	else if (stPacienta == 2){
                		$("#Generiranec2").html("Janez Janez: " + "<i>"+ehrId+"</i>");
                		if ($("#noviGeneriraneci").val() == "")
                		$("#noviGeneriraneci").html("<span style = 'color: green'><h3><b>Generirani EhrId-ji:</b></h3></span>");
                		$("#novGeneriranec2").html("<span><b>Janez Janez: " + ehrId +"</b></span>");
                		
                		var sel = document.getElementById("preberiObstojeciEHR");
						var opt = document.getElementById("2");
						opt.value = ehrId;
						opt.text = "Janez Janez";
						sel.add(opt, null);                		
                	}
                	else if (stPacienta == 3){
                		$("#Generiranec3").html("Gloria Borger: " + "<i>"+ehrId+"</i>");
                		if ($("#noviGeneriraneci").val() == "")
                			$("#noviGeneriraneci").html("<span style = 'color: green'><h3><b>Generirani EhrId-ji:</b></h3></span>");
                		$("#novGeneriranec3").html("<span><b>Gloria Borger: " + ehrId +"</b></span>");
                		
                		var sel = document.getElementById("preberiObstojeciEHR");
						var opt = document.getElementById("3");
						opt.value = ehrId;
						opt.text = "Gloria Borger";
						sel.add(opt, null);
                		
                		
                	}
					return ehrId;
	            },
	            error: function(err) {
		            return "Napaka";
		        }
	        });
	        
	    }
	});	
	
	
}



$(document).ready(function() {

  /**
   * Napolni testne vrednosti (ime, priimek in krvni tlak) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
    $("#kreirajKrvniTlakSistolicni").val(podatki[3]);
    $("#kreirajKrvniTlakDiastolicni").val(podatki[4]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});
});
